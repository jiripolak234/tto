Popis programu TTO.exe
======================
Program slouží jako časovač spouštějící nahrávání zákrytu.

- Program lze používat i samostatně, tj. bez importu času z OW a bez exportu nahrávání do SharpCapu.
- Program se nemusí instalovat, stačí nakopírovat a spustit.
- Pokud se spustí s parametrem "V", tj. TTO.EXE V, potom se zpřístupní zaškrtávátko na vypnutí počítače minutu po skončení zákrytu.


Popis importu času z Occult Watcher
===================================
Nakopirovat OccultWatcher.Addins.dll do adresáře programu OW a spustit ho.
Nic se nestane, ale píše, že je nová aktualizace. Tu stáhnout a objeví se nová volba: Doplňky / Konfigurace doplňků / Udělátka OW viz. konfigurace.PNG.

Na kartu Copy Event Info vložit na první řádek %EventTimeUT%, na druhý řádek %AsteroidName%, další řádky jsou libovolné a program TTC je může zobrazovat jako informační. Příklad:

%EventTimeUT%
%AsteroidName%

Potom už stačí na libovolném zákryt stisknout pravé tlačítko myši a zvolit Zkopírovat do schránky. Na TTC potom stisknout ikonku "Imp. OW" a čas se vloží do programu.



Popis exportu skriptu do SharpCap
=================================
Pokud chcete exportovat skript do SharpCap, tak stisknout tlačítko "Exp. SC". Vygeneruje se skript a uloží do schránky Windows. V SharpCapu potom vybrat v menu Scripting / Show console. Tam přes schránku vložit do spodního okna a spustit zeleným trojúhelníčkem.
Skript nastaví začátek a dobu nahrávání a název planetky. Vše ostatní se bere z aktuálních hodnot těsně před začátkem nahrávání.
Že skript probíhá se pozná jen na ikonách (aktivní červený čtvereček). Po spuštění je možné normálně v SharpCapu pracovat (to je výhoda).
