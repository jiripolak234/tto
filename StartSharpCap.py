import datetime
import time
from datetime import date, time

now = datetime.datetime.utcnow()
### Čas nahrávání v UT ############################# HH,MM,SS ###
later = datetime.datetime.combine(date.today(), time({TTO_StartTime}))
#################################################################
delay=(later-now).total_seconds()
if (delay<0):
  delay = delay + 86400
if (delay>86400):
  delay = delay - 86400
h=int(delay/3600)
m=int((delay-h*3600)/60)
print "Zbývá",int(delay),"s (",h,"hodin,",m,"minut)"
import time
time.sleep(delay)

SharpCap.SelectedCamera = SharpCap.Cameras.Find(lambda x:x.DeviceName == 'QHY174M')
import clr
clr.AddReference("SharpCap")
from SharpCap.UI import CaptureLimitType
SharpCap.SelectedCamera.CaptureConfig.CaptureLimitType = CaptureLimitType.TimeLimited
SharpCap.SelectedCamera.CaptureConfig.CaptureLimitValue = {TTO_Duration}    ### 4*60 s - délka nahrávky v sec
SharpCap.SelectedCamera.CaptureConfig.CaptureSequenceCount = 1
SharpCap.SelectedCamera.CaptureConfig.CaptureSequenceInterval = 1
SharpCap.TargetName = "{TTO_Name}"
SharpCap.SelectedCamera.PrepareToCapture()
SharpCap.SelectedCamera.RunCapture()
